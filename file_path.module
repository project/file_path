<?php
/**
 * @file
 * file_path.module
 */

/**
 * Implements hook_menu().
 */
function file_path_menu() {
  $items = array();

  $items['file_path/autocomplete'] = array(
    'title' => 'File path auto complete',
    'access arguments' => array('access file path auto complete'),
    'page callback' => 'file_path_autocomplete',
  );

  return $items;
}

/**
 * Implements hook_permissions().
 */
function file_path_permissions() {
  return array(
    'access file path auto complete' => array(
      'title' => t('Grant access to file path auto complete'),
    ),
  );
}

/**
 * Implements hook_element_info().
 */
function file_path_element_info() {
  $types['file_path'] = array(
    '#input' => TRUE,
    '#theme' => array('textfield'),
    '#size' => 50,
    '#autocomplete_path' => 'file_path/autocomplete',
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'file_path') . '/js/file_path.js',
        array(
          'type' => 'setting',
          'data' => array('filePath' => array('url' => url('file_path/autocomplete', array('absolute' => TRUE)))),
        ),
      ),
    ),
    '#theme_wrappers' => array('form_element'),
  );
  return $types;
}

/**
 * Return the auto complete path.
 */
function file_path_autocomplete($string = '') {
  $matches = array();
  $string = str_replace("*", "/", $string);
  $explode = explode('/', $string);

  if (!file_Exists($string)) {
    if (count($explode)) {
      // The user is in the first hierarchy of file. Supply the root folder
      // path.
      $string = DRUPAL_ROOT;
    }
  }

  try {
    foreach (new DirectoryIterator($string) as $fileInfo) {
      if ($fileInfo->isDot()) {
        continue;
      }

      $last = end($explode);

      // todo: Don't work with sites/all/modules - check this why.
      if (!empty($last) && !$fileInfo->getFilename() && strpos($fileInfo->getFilename(), $last) === FALSE) {
        // Skipping - The file name don't contain part of the given path.
        continue;
      }

      $path = count($explode) == 1 ? $fileInfo->getFilename() : $fileInfo->getPathname();
      $matches[$path] = $fileInfo->getFilename();
    }

    drupal_json_output($matches);
  } catch (Exception $e) {
    // Save the errors fot later debugging.
    watchdog('file_path', $e->getMessage());
  }
}
